/*
 * Copyright (c) 2021, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef INC_NN_BACKWARD_H_
#define INC_NN_BACKWARD_H_


#ifdef __cplusplus
extern    "C"
{
#endif

void fully_connected_softmax_backward(const float* x,         /* input of the layer */
                                      const float* y,         /* output of the layer (class probabilities) */
                                      uint16_t size_x,        /* dimension of the layer input (x) */
                                      uint16_t size_y,        /* dimension of the layer output (y) */
                                      int16_t label,          /* label (pass negative number to use the max. class as label) */
                                      float* out_gradients);  /* return values: gradients (buffer must be large enough to hold (size_x + 1) * size_y values) */

void fully_connected_softmax_backward_q7(const q7_t* x,         /* input of the layer */
                                         const q7_t* y,         /* output of the layer (for last layer: class probabilities) */
                                         uint16_t size_x,       /* dimension of the layer input (x) */
                                         uint16_t size_y,       /* dimension of the layer output (y) */
                                         uint16_t shift_x,      /* input shift to the right */
                                         int16_t label,         /* label (pass negative number to use the max. class as label) */
                                         float* out_gradients); /* return values: gradients (buffer must be large enough to hold (size_x + 1) * size_y values) */

void cross_entropy_backward(const float* y,     /* actual values */
                            const float* t,     /* expected values */
                            uint16_t size_y,    /* size of the vector y (and t) */
                            float* out_dl_dy);  /* return value: partial derivatives of the loss function against the y values */

void softmax_backward(const float* dl_dy,   /* partial derivatives of the loss function against the outputs */
                      const float* y,       /* outputs of the softmax layer */
                      uint16_t size_y,      /* size of the vector y */
                      float* out_dl_dz);    /* return value: partial derivatives of the loss function against the inputs of the softmax layer */

/* return value: partial derivative of the loss function against the specified weight */
float fully_connected_backward(const float* dl_dz,  /* partial derivatives of the loss function against the outputs of the FC layer */
                               const float* x,      /* inputs of the FC layer */
                               uint16_t size_x,     /* size of the vector x */
                               uint16_t w_i,        /* row index of the weight */
                               uint16_t w_j);       /* column index of the weight (if > size_x, the partial derivative for the bias will be returned instead) */


#ifdef __cplusplus
}
#endif

#endif /* INC_NN_BACKWARD_H_ */
