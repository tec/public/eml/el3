/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <inttypes.h>
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */
#define PIN_TOGGLE(p)     HAL_GPIO_TogglePin(p##_GPIO_Port, p##_Pin)
#define PIN_SET(p)        p##_GPIO_Port->BSRR = p##_Pin
#define PIN_CLR(p)        p##_GPIO_Port->BRR = p##_Pin
#define PIN_GET(p)        ((p##_GPIO_Port->IDR & p##_Pin) != 0)
#define PIN_STATE(p)      ((p##_GPIO_Port->ODR & p##_Pin) != 0)
#define PIN_XOR(p)        PIN_TOGGLE(p)
#define UART_PRINT(s)     uart_print(s, 0)
#define UART_PRINTF(...)  uart_printf(__VA_ARGS__)
#define UART_PRINTLN(s)   uart_println(s, 0)
#define FATAL_ERROR(s)    if (s) { uart_println(s, 0); } PIN_SET(LED_RED); while(1)

#define FOREACH(data, len, action)  for (unsigned int i_##data = 0; i_##data < len; i_##data++) { data[i_##data] action; }

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
bool uart_print(const char* str, uint16_t len);
bool uart_println(const char* str, uint16_t len);
void uart_printf(const char * str, ...);
uint32_t crc32(const uint8_t* data, uint32_t num_bytes, uint32_t seed);
void delay(volatile uint32_t count);

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LED3_Pin GPIO_PIN_14
#define LED3_GPIO_Port GPIOB
#define LED1_Pin GPIO_PIN_7
#define LED1_GPIO_Port GPIOC
#define LED2_Pin GPIO_PIN_7
#define LED2_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define LED_GREEN_Pin       LED1_Pin
#define LED_GREEN_GPIO_Port LED1_GPIO_Port
#define LED_BLUE_Pin        LED2_Pin
#define LED_BLUE_GPIO_Port  LED2_GPIO_Port
#define LED_RED_Pin         LED3_Pin
#define LED_RED_GPIO_Port   LED3_GPIO_Port

extern UART_HandleTypeDef hlpuart1;
extern TIM_HandleTypeDef htim2;
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
