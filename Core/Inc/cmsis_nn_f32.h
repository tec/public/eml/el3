
#ifndef INC_CMSIS_NN_F32_H_
#define INC_CMSIS_NN_F32_H_


#include "arm_math.h"
#include "arm_nnfunctions.h"


#ifdef __cplusplus
extern    "C"
{
#endif


void fully_connected_f32(const float* x,
                         const float* w,
                         const uint16_t cols,   // = size of x
                         const uint16_t rows,   // = size of y
                         const float* b,
                         float* out_y);

void softmax_f32(const float* vec_in,
                 const uint16_t dim_vec,
                 float* p_out);

void batch_norm_f32(const float* v,
                    const uint16_t dim,
                    float* out_v);

void conv2D_nonsquare_f32(const float * Im_in,
                          const uint16_t dim_im_in_x,
                          const uint16_t dim_im_in_y,
                          const uint16_t ch_im_in,
                          const float * wt,
                          const uint16_t ch_im_out,
                          const uint16_t dim_kernel_x,
                          const uint16_t dim_kernel_y,
                          const uint16_t padding_x,
                          const uint16_t padding_y,
                          const uint16_t stride_x,
                          const uint16_t stride_y,
                          const float * bias,
                          float * Im_out,
                          const uint16_t dim_im_out_x,
                          const uint16_t dim_im_out_y);


#ifdef __cplusplus
}
#endif

#endif /* INC_CMSIS_NN_F32_H_ */
