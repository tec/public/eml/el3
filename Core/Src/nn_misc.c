/*
 * Copyright (c) 2021, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * misc NN helper functions
 */

#include "app.h"


void float_to_int8(const float* input,
                   int8_t* output,
                   uint16_t count,
                   uint16_t left_shift)
{
  for (uint16_t i = 0; i < count; i++) {
    float val = input[i] * (1 << left_shift);
    if (val > 127.0f) {
      output[i] = 127;
    } else if (val < -128.0f) {
      output[i] = -128;
    } else {
      output[i] = (int8_t)val;
    }
  }
}


void int8_to_float(const int8_t* input,
                   float* output,
                   uint16_t count,
                   uint16_t right_shift)
{
  for (uint16_t i = 0; i < count; i++) {
    if (right_shift) {
      output[i] = (float)input[i] / (1 << right_shift);
    } else {
      output[i] = input[i];
    }
  }
}


// automatic quantization, returns the number of decimal places
int8_t quantize_f32_to_int8(const float* v,
                            uint16_t dim,
                            int8_t* out_v)
{
  // first, find the max value
  float max_val = v[0];
  for (uint16_t i = 1; i < dim; i++) {
    float abs_val = fabsf(v[i]);
    if (abs_val > max_val) {
      max_val = abs_val;
    }
  }

  // find the number of required integer places
  int16_t int_bits = ceilf(log2f(max_val));
  if (int_bits > 7) {
    int_bits = 7;
  }

  // convert to int8
  for (uint16_t i = 0; i < dim; i++) {
    float val = v[i] * (1 << (7 - int_bits));  // int_bits can be negative
    if (val >= 127) {
      out_v[i] = 127;
    } else if (out_v[i] <= -128) {
      out_v[i] = -128;
    } else {
      out_v[i] = val;
    }
  }

  return int_bits;
}
