/*
 * 32-bit implementations of CMSIS-NN functions (swap-in replacements)
 */

#include "app.h"


void fully_connected_f32(const float* x,
                         const float* w,
                         const uint16_t cols,   // = size of x
                         const uint16_t rows,   // = size of y
                         const float* b,
                         float* out_y)
{
  for (uint32_t i = 0; i < rows; i++) {
    float s = b[i];
    for (uint32_t j = 0; j < cols; j++) {
      s += x[j] * w[i * cols + j];
    }
    out_y[i] = s;
  }
}


void softmax_f32(const float* v,
                 const uint16_t dim,
                 float* out_v)
{
  float    base = v[0];
  float    sum  = 0;
  uint16_t i;

  for (i = 1; i < dim; i++) {
    if (v[i] > base) {
      base = v[i];
    }
  }
  base = base - 80.0f;   // exp(88) is close to the max. possible value for float

  for (i = 0; i < dim; i++) {
    if (v[i] > base) {
      out_v[i] = exp(v[i] - base);
      sum += out_v[i];
    } else {
      out_v[i] = 0;
    }
  }
  for (i = 0; i < dim; i++) {
    out_v[i] /= sum;
  }
}


// based on arm_convolve_HWC_q7_basic_nonsquare()
void conv2D_nonsquare_f32(const float* Im_in,
                          const uint16_t dim_im_in_x,
                          const uint16_t dim_im_in_y,
                          const uint16_t ch_im_in,
                          const float* wt,
                          const uint16_t ch_im_out,
                          const uint16_t dim_kernel_x,
                          const uint16_t dim_kernel_y,
                          const uint16_t padding_x,
                          const uint16_t padding_y,
                          const uint16_t stride_x,
                          const uint16_t stride_y,
                          const float* bias,
                          float* Im_out,
                          const uint16_t dim_im_out_x,
                          const uint16_t dim_im_out_y)
{
  for (uint16_t i = 0; i < ch_im_out; i++) {
    for (uint16_t j = 0; j < dim_im_out_y; j++) {
      for (uint16_t k = 0; k < dim_im_out_x; k++) {
        float conv_out = bias[i];
        for (uint16_t m = 0; m < dim_kernel_y; m++) {
          for (uint16_t n = 0; n < dim_kernel_x; n++) {
            // if-for implementation
            int16_t in_row = stride_y * j + m - padding_y;
            int16_t in_col = stride_x * k + n - padding_x;
            if (in_row >= 0 && in_col >= 0 && in_row < dim_im_in_y && in_col < dim_im_in_x) {
              for (uint16_t l = 0; l < ch_im_in; l++) {
                conv_out += Im_in[(in_row * dim_im_in_x + in_col) * ch_im_in + l] *
                            wt[i * ch_im_in * dim_kernel_y * dim_kernel_x +
                            (m * dim_kernel_x + n) * ch_im_in + l];
              }
            }
          }
        }
        Im_out[i + (j * dim_im_out_x + k) * ch_im_out] = conv_out;
      }
    }
  }
}
