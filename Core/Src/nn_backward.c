/*
 * Copyright (c) 2021, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * NN backward functions
 */

#include "app.h"


// calculates the partial derivatives of the loss function (using cross entropy and assuming one hot) against the weights and bias for a FC + SM layer
void fully_connected_softmax_backward(const float* x,         // input of the layer
                                      const float* y,         // output of the layer (for last layer: class probabilities)
                                      uint16_t size_x,        // dimension of the layer input (x)
                                      uint16_t size_y,        // dimension of the layer output (y)
                                      int16_t label,          // label (pass negative number to use the max. class as label)
                                      float* out_gradients)   // return values: gradients (buffer must be large enough to hold (size_x + 1) * size_y values)
{
  uint16_t max_idx = label;

  if (label < 0 || label >= size_y) {
    // determine the max value in the output vector (which contains the class probabilities)
    for (uint16_t idx = 1; idx < size_y; idx++) {
      if (y[idx] > y[max_idx]) {
        max_idx = idx;
      }
    }
  }
  // calculate the partial derivatives of the error term against all weights (go through all input/output pairs)
  for (uint16_t i = 0; i < size_y; i++) {
    float t = (i == max_idx) ? 1.0f : 0.0f;
    for (uint16_t j = 0; j < size_x; j++) {
      // update gradient
      out_gradients[i * size_x + j] += (y[i] - t) * x[j];
    }
    // update gradient
    out_gradients[size_y * size_x + i] += (y[i] - t);
  }
}


// calculates the partial derivatives of the loss function (using cross entropy and assuming one hot) against the weights and bias for a FC + SM layer
void fully_connected_softmax_backward_q7(const q7_t* x,         // input of the layer
                                         const q7_t* y,         // output of the layer (for last layer: class probabilities)
                                         uint16_t size_x,       // dimension of the layer input (x)
                                         uint16_t size_y,       // dimension of the layer output (y)
                                         uint16_t shift_x,      // input shift to the right
                                         int16_t label,         // label (pass negative number to use the max. class as label)
                                         float* out_gradients)  // return values: gradients (buffer must be large enough to hold (size_x + 1) * size_y values)
{
  float    f_shift = 1.0f / (float)(1 << shift_x);
  uint16_t max_idx = label;

  if (label < 0 || label >= size_y) {
    // determine the max value in the output vector (which contains the class probabilities)
    for (uint16_t idx = 1; idx < size_y; idx++) {
      if (y[idx] > y[max_idx]) {
        max_idx = idx;
      }
    }
  }
  // calculate the partial derivatives of the error term against all weights (go through all input/output pairs)
  for (uint16_t i = 0; i < size_y; i++) {
    float t = (i == max_idx) ? 1.0f : 0.0f;
    float y_i = y[i] / 128.0f;
    for (uint16_t j = 0; j < size_x; j++) {
      // update gradient
      out_gradients[i * size_x + j] += (y_i - t) * (float)x[j] * f_shift;
    }
    // update gradient
    out_gradients[size_y * size_x + i] += (y_i - t);
  }
}


void cross_entropy_backward(const float* y,     // actual values
                            const float* t,     // expected values
                            uint16_t size_y,    // size of the vector y (and t)
                            float* out_dl_dy)   // return value: partial derivatives of the loss function against the y values
{
  for (uint16_t i = 0; i < size_y; i++) {
    if (y[i] == 0) {
      out_dl_dy[i] = 0;
    } else {
      out_dl_dy[i] = -t[i] / y[i];
    }
  }
}


void softmax_backward(const float* dl_dy,   // partial derivatives of the loss function against the outputs
                      const float* y,       // outputs of the softmax layer
                      uint16_t size_y,      // size of the vector y
                      float* out_dl_dz)     // return value: partial derivatives of the loss function against the inputs of the softmax layer
{
  for (uint16_t i = 0; i < size_y; i++) {
    // calculate the derivative of L against z[i]
    out_dl_dz[i] = 0;
    for (uint16_t j = 0; j < size_y; j++) {
      if (i == j) {
        out_dl_dz[i] += dl_dy[j] * (y[j] - y[j] * y[j]);
      } else {
        out_dl_dz[i] += dl_dy[j] * (-y[i] * y[j]);
      }
    }
  }
}


// return value: partial derivative of the loss function against the specified weight
float fully_connected_backward(const float* dl_dz,  // partial derivatives of the loss function against the outputs of the FC layer
                               const float* x,      // inputs of the FC layer
                               uint16_t size_x,     // size of the vector x
                               uint16_t w_i,        // row index of the weight
                               uint16_t w_j)        // column index of the weight (if > size_x, the partial derivative for the bias will be returned instead)
{
  if (w_j > size_x) {
    return dl_dz[w_i];          // bias
  }
  return dl_dz[w_i] * x[w_j];   // weight
}
