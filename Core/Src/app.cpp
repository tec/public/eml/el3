/*
 * Copyright (c) 2021, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * main application
 */

#include "main.h"
#include "app.h"
#include "KWS_DS_CNN/kws_ds_cnn.h"


// packet definitions for data transfer (audio samples) via serial port
#define FRAMING_BYTE          0x7e
#define ESCAPE_BYTE           0x7d
#define RX_BUFFER_SIZE        1024
#define UART_PKT_HDR_LEN      10
#define NUM_AUDIO_SAMPLES     16000                     // 1s duration, 16kHz sampling rate
#define AUDIO_BUFFER_SIZE     (NUM_AUDIO_SAMPLES * 2)   // 16-bit samples
#define CMD_PRINT_GRADIENTS   -2
#define CMD_PRINT_WEIGHTS     -3
#define CMD_PRINT_QUANTIZED   -4

typedef struct __attribute__((packed))
{
  uint16_t seq_no;    // sequence number
  uint16_t offset;    // offset in number of samples
  uint32_t crc;       // CRC32 over the payload
  int16_t  label;     // label (class index, neg. value = no label)
  uint8_t  payload[RX_BUFFER_SIZE - UART_PKT_HDR_LEN];
} uart_pkt_t;


// data structures required for the floating-point implementation of the last layer
float ll_input[LL_INPUT_DIM];
float ll_weights[LL_INPUT_DIM * LL_OUTPUT_DIM];
float ll_bias[LL_OUTPUT_DIM];
float ll_output[LL_OUTPUT_DIM];
float ll_gradients[(LL_INPUT_DIM + 1) * LL_OUTPUT_DIM] = { 0 };
bool  ll_use_float = false;   // whether to use floating point operations for the last layer (application-controlled setting)

static int16_t audio_buffer[NUM_AUDIO_SAMPLES] = { 0 };
static int8_t  rcvd_label;


static bool APP_ParseData(uart_pkt_t* pkt, uint32_t len)
{
  static uint16_t last_seq_no = 0;

  // check payload length
  if (len < UART_PKT_HDR_LEN) {
    UART_PRINTLN("error: packet too small");
    return false;
  }
  // calculate and compare the CRC
  uint32_t payload_len = len - UART_PKT_HDR_LEN;
  if (payload_len > 0) {
    uint32_t crc_calc = crc32(pkt->payload, payload_len, 0);
    if (crc_calc != pkt->crc) {
      UART_PRINTF("error: invalid CRC detected (packet dropped)\r\n", crc_calc, pkt->crc);
      return false;
    }
  }
  // check sequence number
  if (pkt->seq_no != 0 && (uint16_t)(pkt->seq_no - last_seq_no) > 1) {
    UART_PRINTF("error: gap in sequence number detected (current: %u, previous: %u)\r\n", pkt->seq_no, last_seq_no);
  }
  last_seq_no = pkt->seq_no;
  rcvd_label  = pkt->label;

  // check offset and store the payload
  if ((payload_len > 0) && ((pkt->offset * 2 + payload_len) <= AUDIO_BUFFER_SIZE)) {
    memcpy(&audio_buffer[pkt->offset], pkt->payload, payload_len);
  }
  if ((pkt->offset * 2 + payload_len) >= AUDIO_BUFFER_SIZE) {
    // buffer full: abort input parsing and run inference
    return true;
  }

  return false;
}


static void APP_ReceiveData(void)
{
  static uint8_t rx_buffer[RX_BUFFER_SIZE + 1] __attribute__ ((aligned (32)));

  bool     start_found = false;
  bool     escaped     = false;
  uint32_t len         = 0;

  // read data from serial port
  while (1) {
    // wait for the next character
    uint8_t rcv = 0;
    HAL_StatusTypeDef res = HAL_UART_Receive(&hlpuart1, &rcv, 1, 100);
    if (res == HAL_OK) {
      // character received
      if (!escaped && (rcv == ESCAPE_BYTE)) {
        escaped = true;
      } else {
        if (!start_found) {
          len = 0;
          if (!escaped && (rcv == FRAMING_BYTE)) {
            start_found = true;
          }
        } else {
          if (!escaped && (rcv == FRAMING_BYTE)) {
            // frame start / end
            if (len > 0) {
              rx_buffer[len] = 0;
              if (APP_ParseData((uart_pkt_t*)rx_buffer, len)) {
                break;    // abort data reception
              }
              len         = 0;
              start_found = false;
            } // else: invalid packet length -> ignore
          } else {
            rx_buffer[len++] = rcv;
          }
          // check for buffer overflow
          if (len >= RX_BUFFER_SIZE) {
            UART_PRINTLN("error: packet dropped (too long)");
            len         = 0;
            start_found = false;
          }
        }
        escaped = false;
      }
    } else {
      if (res != HAL_TIMEOUT) {
        UART_PRINTLN("HAL driver error");
      }
      if (__HAL_UART_GET_FLAG(&hlpuart1, UART_FLAG_ORE)) {
        // overrun flag is set: clear it
        __HAL_UART_CLEAR_OREFLAG(&hlpuart1);
      }
    }
  }
}


void APP_PrintFloatArray(const float* v, uint16_t rows, uint16_t cols)
{
  for (uint16_t i = 0; i < rows; i++) {
    for (uint16_t j = 0; j < cols; j++) {
      UART_PRINTF("%.3f ", v[i * LL_INPUT_DIM + j]);
    }
    UART_PRINTF("\r\n");
  }
}

void APP_PrintInt8Array(const int8_t* v, uint16_t rows, uint16_t cols)
{
  for (uint16_t i = 0; i < rows; i++) {
    for (uint16_t j = 0; j < cols; j++) {
      UART_PRINTF("%d ", v[i * LL_INPUT_DIM + j]);
    }
    UART_PRINTF("\r\n");
  }
}


#ifdef __cplusplus
extern "C" {
#endif


bool APP_Init(void)
{
  PIN_SET(LED_GREEN);
  UART_PRINTLN("STM32L4 initialized, ready to receive data");

  return true;
}


void APP_Run(void)
{
  static const char* output_class[LL_OUTPUT_DIM] = { "silence", "unknown", "yes", "no", "up", "down", "left", "right", "on", "off", "stop", "go" };
  uint16_t gradients_count = 0;

  // initialize the NN
  static KWS_DS_CNN kws(audio_buffer);

  while (1) {
    // receive an audio sample (waveform)
    APP_ReceiveData();    // blocking call, will only return once a valid audio sample has been received

    PIN_SET(LED_BLUE);

    // if a label has been received, then the last layer should be run in floating point
    ll_use_float = (rcvd_label >= 0 && rcvd_label < LL_OUTPUT_DIM);
    if (ll_use_float) {
      PIN_SET(LED_RED);     // indicate with LED that learning is enabled
    }

    // perform MFCC feature extraction and run inference
    uint32_t starttime = __HAL_TIM_GET_COUNTER(&htim2);
    kws.extract_features();
    kws.classify();
    uint32_t stoptime = __HAL_TIM_GET_COUNTER(&htim2);
    UART_PRINTF("processing time: %lums\r\n", (stoptime - starttime) / 1000);

    // print results
    int max_idx = kws.get_top_class(kws.output);
    UART_PRINTF("detected '%s' (%d%%)\r\n", output_class[max_idx], ((int)kws.output[max_idx] * 100 / 128));
    if (rcvd_label >= 0 && rcvd_label < LL_OUTPUT_DIM) {
      UART_PRINTF("actual label is '%s'\r\n", output_class[rcvd_label]);
    }
    if (ll_use_float) {
      UART_PRINTF("class probability (float): %.1f%%\r\n", ll_output[max_idx] * 100.0f);
    }

    // only calculate gradient if a valid label has been provided
    if (rcvd_label >= 0 && rcvd_label < LL_OUTPUT_DIM) {

      // calculate gradients for the last layer
      fully_connected_softmax_backward(ll_input, ll_output, LL_INPUT_DIM, LL_OUTPUT_DIM, rcvd_label, ll_gradients);
      gradients_count++;

      // update weights
      if (gradients_count >= BATCH_SIZE) {

        float f_learn = LEARNING_RATE / gradients_count;

        for (uint32_t i = 0; i < LL_INPUT_DIM * LL_OUTPUT_DIM; i++) {
          ll_weights[i] -= f_learn * ll_gradients[i];
        }
        for (uint32_t i = 0; i < LL_OUTPUT_DIM; i++) {
          ll_bias[i] -= f_learn * ll_gradients[LL_INPUT_DIM * LL_OUTPUT_DIM + i];
        }
        // clear gradients
        memset(ll_gradients, 0, sizeof(ll_gradients));
        gradients_count = 0;
        UART_PRINTLN("weights and bias updated");
      }
      // re-quantize weights and bias
      float_to_int8(ll_weights, DS_CNN::final_fc_wt, LL_INPUT_DIM * LL_OUTPUT_DIM, FINAL_FC_OUT_RSHIFT);
      float_to_int8(ll_bias, DS_CNN::final_fc_bias, LL_OUTPUT_DIM, FINAL_FC_OUT_RSHIFT - FINAL_FC_BIAS_LSHIFT);

    // for debugging: handle commands (the 'label' field in the UART packet is used to transmit commands)
    } else if (rcvd_label == CMD_PRINT_GRADIENTS) {
      // on demand: print gradients
      UART_PRINTLN("weight gradients:");
      APP_PrintFloatArray(ll_gradients, LL_OUTPUT_DIM, LL_INPUT_DIM);
      UART_PRINTLN("bias gradients:");
      APP_PrintFloatArray(&ll_gradients[LL_OUTPUT_DIM * LL_INPUT_DIM], 1, LL_OUTPUT_DIM);

    } else if (rcvd_label == CMD_PRINT_WEIGHTS) {
      // on demand: print weights and bias
      UART_PRINTLN("weights:");
      APP_PrintFloatArray(ll_weights, LL_OUTPUT_DIM, LL_INPUT_DIM);
      UART_PRINTLN("bias:");
      APP_PrintFloatArray(ll_bias, 1, LL_OUTPUT_DIM);

    } else if (rcvd_label == CMD_PRINT_QUANTIZED) {
      // on demand: print quantized weights and bias
      UART_PRINTLN("weights:");
      APP_PrintInt8Array(DS_CNN::final_fc_wt, LL_OUTPUT_DIM, LL_INPUT_DIM);
      UART_PRINTLN("bias:");
      APP_PrintInt8Array(DS_CNN::final_fc_bias, 1, LL_OUTPUT_DIM);
    }

    PIN_CLR(LED_RED);
    PIN_CLR(LED_BLUE);
  }
}


#ifdef __cplusplus
}
#endif
