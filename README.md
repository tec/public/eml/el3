# eL<sup>3</sup> - embedded Last-Layer Learning

This repository contains code to demonstrate on-device keyword spotting (KWS) utilizing a depthwise-separable convolutional neural network (DS-CNN) with last-layer adaptation. The code runs on an STM32L4 ultra low-power microcontroller and is based on [*Keyword spotting for Microcontrollers*](https://github.com/ARM-software/ML-KWS-for-MCU) provided by the authors of the corresponding paper [^1].
We have extended the existing code to do not only inference, but also on-device last-layer training.  
The idea of last-layer adaptation is to deploy a pre-trained network on the embedded device and let it *adapt* to its new environment by adjusting the weights of the last (fully-connected) layer to improve the detection accuracy. The properties of the audio signal may be different for each device depending on the deployment location and sensor characteristics. For example, there could be reverberation or other kinds of signal-altering effect that wasn't accounted for during training (either due to a lack of training samples or capacity limits of the neural network).


## Getting started: run on-device inference

The code in this repository runs on a NUCLEO-L496ZG development board with the STM32L496 MCU, but can easily be ported to other platforms as well. The following steps explain how to deploy the pre-trained network on the development board and then send raw audio samples to the MCU for inference.

1. Install the [STM32CubeIDE](https://www.st.com/en/development-tools/stm32cubeide.html) and open this project in the IDE.
1. In the IDE, open the file [`kws-ll-training.ioc`](kws-ll-training.ioc) which contains the MCU configuration. Use the button *'Device Configuration Tool Code Generation'* to initiate the code generation process.
1. Make sure the [NUCLEO-L496ZG](https://www.st.com/en/evaluation-tools/nucleo-l496zg.html) development board is connected to your computer via USB and then compile and flash the application.
1. Use the provided Python script [`send_audiodata.py`](Scripts/send_audiodata.py) to transfer an audio file to the MCU via the serial port. :
   ```
   $ ./Scripts/send_audiodata.py --filename test_audiosample_up.wav --port /dev/ttyACM0
   connected to /dev/ttyACM0
   processing time: 249ms
   detected 'up' (99%)
   ```
   Notes:
   * Don't forget to adjust the port to the actual name of the serial device (depends on your operating system).
   * The on-board ST-Link debugger will automatically provide a serial device once the development board is plugged into your computer. There is no need to use a separate serial-to-USB converter.
   * The audio file must be in uncompressed wave format (16-bit, 16kHz) and one second long. Shorter files will be zero-padded, longer files truncated. A sample audio file is included in this repository.


### Inference vs training

For last-layer training, the correct audio label must be sent along with the audio sample. To do so, the parameter `--send_labels` can be set:  
```
./Scripts/send_audiodata.py --filename test_audiosample_up.wav --label up --send_labels --port /dev/ttyACM0
```
If no label is sent, the MCU will only perform inference.


### Neural network

A small, pre-trained DS-CNN with five convolutional and one fully-connected layer is used in this code. In a preprocessing step, the MFCC features of the audio data are extracted and quantized. The resulting matrix of dimension 10x49 (10 MFCC coefficients and 49 frames) is then fed to the input layer of the DS-CNN. The output of the network is a vector of size 12, i.e. there are 12 output classes: *silence*, *unknown*, *yes*, *no*, *up*, *down*, *left*, *right*, *on*, *off*, *stop* and *go*.  
The weights and biases of the pre-trained network are located in the file [`ds_cnn_weights.h`](Third_Party/KWS/NN/DS_CNN/ds_cnn_weights.h). All other neural network configuration parameters such as the required scaling factors for each layer (bit shifts) are stored in [`ds_cnn.h`](Third_Party/KWS/NN/DS_CNN/ds_cnn.h). The network itself is implemented in [`ds_cnn.cpp`](Third_Party/KWS/NN/DS_CNN/ds_cnn.cpp) with the [CMSIS-NN](https://github.com/ARM-software/CMSIS) library.
Note that the code for the DS-CNN and MFCC extraction is provided by [ARM](https://github.com/ARM-software/ML-KWS-for-MCU), but we applied minor changes in order to make it work with the last-layer training.


## Dataset

A speech command dataset is available on the [tensorflow website](https://www.tensorflow.org/datasets/catalog/speech_commands). You can use the script [`create_augmented_dataset.py`](Scripts/create_augmented_dataset.py) to download and then augment the speech commands dataset with a reverberation effect. Note that the script requires [SoX](https://sourceforge.net/projects/sox/).  
Once you have generated the augmented dataset, you can then use [`generate_filelist.py`](Scripts/generate_filelist.py) to compose a list of filename and label pairs for the training, validation and testing sets.


## How to: last-layer training

The following steps describe how to do last-layer training on the device:

1. Create an augmented dataset, e.g. with the script [`create_augmented_dataset.py`](Scripts/create_augmented_dataset.py).  
1. Divide the dataset into a training, validation and testing set with the script [`generate_filelist.py`](Scripts/generate_filelist.py).
1. Make sure the code is deployed on the development board as described above and that the board connected to your computer via USB.  
1. Start the training process:
   ```
   ./Scripts/send_audiodata.py --listfile training.csv --send_labels
   ```
   Remark: Depending on the size of the training set, this may take a long time (approx. 0.9s per audio file, resulting in a total duration of ~5.5h for 23k files).
1. Once the training is completed, you can run the testing dataset to check the accuracy of the adapted network:
   ```
   ./Scripts/send_audiodata.py --listfile testing.csv
   ```
   Since the training dataset contains by default only 20% of all files, it takes only one third of the training time to run.
1. If you want to retrieve the adapted (re-quantized) weights, you can send the following command to the MCU:
   ```
   ./Scripts/send_audiodata.py --command print_quantized
   ```
   Note: there is also a command to print the floating point weights and gradients.  

### Results
The testing accuracy of the unmodified network (i.e. prior to adaptation) is 65.5% for the dataset augmented with the reverb effect. After last-layer training with batch size 32 and learning rate 0.001 (one run only), the testing accuracy has increased to 77.5%. Training the last layer takes quite a long time, mostly due to the transmission time over the serial port.  
Remark: the batch size and learning rate for the last-layer training can be configured in [`app.h`](Core/Inc/app.h).


## What about other types of neural networks?

The code in this repository currently only contains an implementation for one specific neural network (DS-CNN). Here are some pointers on what needs to be done in order to add support for a different network:

* MCU code:
  * Implement your neural network in `Third_Party/KWS/NN/[your_network]/[your_network].c` using function calls of the CMSIS-NN library.
  * Store the weights of your pre-trained neural network in `Third_Party/KWS/NN/[your_network]_weights.h`. Note: for this you will need to quantize the weights, see instructions below.
  * Don't forget to create a wrapper class in `Third_Party/KWS/KWS/KWS_[your_network]/kws_[your_network].c`.
  * Include the header file for the wrapper class in the main application file [`app.cpp`](Core/Src/app.cpp) and create an class instance in `APP_Run()`.
* Python code:
  * Extend [`models.py`](https://github.com/ARM-software/ML-KWS-for-MCU/blob/master/models.py) with a Python implementation of your model.
  * Implement quantization for your model in [`quant_models.py`](https://github.com/ARM-software/ML-KWS-for-MCU/blob/master/quant_models.py).


## Quantizing a trained network

In case you need to train your own network, the weights and activations must be quantized prior to deployment to the MCU. For this you can follow the [quantization guide](https://github.com/ARM-software/ML-KWS-for-MCU/blob/master/Deployment/Quant_guide.md) provided by the authors of the KWS code. To summarize:
1. Fuse the batch normalization layers:  
   ```
   python fold_batchnorm.py --model_architecture ds_cnn --model_size_info 5 64 10 4 2 2 64 3 3 1 1 64 3 3 1 1 64 3 3 1 1 64 3 3 1 1 --dct_coefficient_count 10 --window_size_ms 40 --window_stride_ms 20 --checkpoint /path/to/checkpoint/best/ds_cnn_xyz
   ```
1. Run `quant_test.py` repeatedly. Start with the following command:  
   ```
   python quant_test.py --model_architecture ds_cnn --model_size_info 5 64 10 4 2 2 64 3 3 1 1 64 3 3 1 1 64 3 3 1 1 64 3 3 1 1 --dct_coefficient_count 10 --window_size_ms 40 --window_stride_ms 20 --checkpoint /path/to/checkpoint/best/ds_cnn_xyz_bnfused --act_max 32 0 0 0 0 0 0 0 0 0 0 0
   ```
   We are interested in the `--act_max` parameter here. It will basically set the clamping range for the inputs/outputs of all layers. In the example above, the inputs to the first layer will be clamped to the range -32 to 32, but all other inputs/outputs are left unquantized (`0`). Run the script for different values (powers of two between 1 and 128) and choose the one that yields the best performance. Once the value has been fixed, move on to the next one (which is the output of the first layer). Continue this iterative process until you have found the best values for all activations.  
   Note: for this step it is sufficient to only use the test dataset and comment out the [training/validation](https://github.com/ARM-software/ML-KWS-for-MCU/blob/master/quant_test.py#L126) datasets to accelerate the process.
1. Once you have determined the best sequence for `--act_max`, copy the values into the script [`calc_ds-cnn_bitshifts.py`](Scripts/calc_ds-cnn_bitshifts.py). In addition, copy the output of `quant_test.py` into the variable `quant_test_output` in the same script (actually only the portion which contains the information about the decimal places for the DS_CNN weights and biases is of interest). Run `calc_ds-cnn_bitshifts.py` to get the required bitshifts for each layer. The output should look something like this:  
   ```
   layer 0 input format: Q6.1
   CONV1_OUT_RSHIFT: 6
   CONV1_BIAS_LSHIFT: 3
   layer 1 input format: Q2.5
   CONV2_DS_OUT_RSHIFT: 5
   CONV2_DS_BIAS_LSHIFT: 4
   layer 2 input format: Q2.5
   CONV2_PW_OUT_RSHIFT: 7
   CONV2_PW_BIAS_LSHIFT: 6
   layer 3 input format: Q2.5
   CONV3_DS_OUT_RSHIFT: 6
   CONV3_DS_BIAS_LSHIFT: 4
   layer 4 input format: Q3.4
   CONV3_PW_OUT_RSHIFT: 6
   CONV3_PW_BIAS_LSHIFT: 5
   layer 5 input format: Q2.5
   CONV4_DS_OUT_RSHIFT: 7
   CONV4_DS_BIAS_LSHIFT: 5
   layer 6 input format: Q3.4
   CONV4_PW_OUT_RSHIFT: 8
   CONV4_PW_BIAS_LSHIFT: 6
   layer 7 input format: Q4.3
   CONV5_DS_OUT_RSHIFT: 7
   CONV5_DS_BIAS_LSHIFT: 3
   layer 8 input format: Q5.2
   CONV5_PW_OUT_RSHIFT: 5
   CONV5_PW_BIAS_LSHIFT: 3
   skipping pooling layer (left shift: 2)
   layer 9 input format: Q1.6
   FINAL_FC_OUT_RSHIFT: 8
   FINAL_FC_BIAS_LSHIFT: 3
   final layer output format: Q3.4
   ```
   These values need to be copied to the respective defines in [`ds_cnn.h`](Third_Party/KWS/NN/DS_CNN/ds_cnn.h).


## Third-party software

| Component                                                                                | License                                                  | Location in repo               | Contained in repo  |
|------------------------------------------------------------------------------------------|----------------------------------------------------------|--------------------------------|--------------------|
| [CMSIS](https://github.com/ARM-software/CMSIS) (by ARM)                                  | Apache-2.0                                               | `Third_Party/CMSIS`            | yes                |
| [ML KWS for MCU](https://github.com/ARM-software/ML-KWS-for-MCU) (by ARM)                | Apache-2.0                                               | `Third_Party/KWS`              | yes                |
| [STM32CubeIDE generated code](https://www.st.com/en/development-tools/stm32cubeide.html) | Ultimate Liberty License SLA0044/SLA0048 or BSD 3-Clause | `Drivers`, `Startup`, `Core`   | partially          |


## References

[^1]: [*Hello Edge: Keyword Spotting on Microcontrollers*](https://arxiv.org/abs/1711.07128), Zhang et al. (2018)
