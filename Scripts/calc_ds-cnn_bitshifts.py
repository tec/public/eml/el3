#!/usr/bin/env python3

"""
Copyright (c) 2021, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# quantization of activations: calculates the required bitshifts (output and bias) for all DS-CNN layers

import re
import math
import sys


# ideal clamping range for all layers (determined by repeatedly running quant_test.py with varying --act_max parameters)
act_max = [32, 4, 4, 4, 8, 4, 8, 16, 32, 8, 2, 8 ]

# paste output of quant_test.py here:
quant_test_output = """
DS-CNN_conv_1_weights_0 number of wts/bias: (10, 4, 1, 64) dec bits: 10 max: (0.08496094,0.08481408) min: (-0.087890625,-0.0875945)
DS-CNN_conv_1_biases_0 number of wts/bias: (64,) dec bits: 8 max: (0.4765625,0.476902) min: (-0.2421875,-0.24271365)
DS-CNN_conv_ds_1_dw_conv_depthwise_weights_0 number of wts/bias: (3, 3, 64, 1) dec bits: 5 max: (3.78125,3.7883532) min: (-2.5625,-2.5589187)
DS-CNN_conv_ds_1_dw_conv_biases_0 number of wts/bias: (64,) dec bits: 6 max: (1.203125,1.2012975) min: (-0.828125,-0.82413685)
DS-CNN_conv_ds_1_pw_conv_weights_0 number of wts/bias: (1, 1, 64, 64) dec bits: 7 max: (0.6484375,0.6446607) min: (-0.75,-0.7501902)
DS-CNN_conv_ds_1_pw_conv_biases_0 number of wts/bias: (64,) dec bits: 6 max: (1.34375,1.3438154) min: (-0.90625,-0.91268826)
DS-CNN_conv_ds_2_dw_conv_depthwise_weights_0 number of wts/bias: (3, 3, 64, 1) dec bits: 5 max: (2.75,2.7476761) min: (-1.96875,-1.963935)
DS-CNN_conv_ds_2_dw_conv_biases_0 number of wts/bias: (64,) dec bits: 6 max: (1.390625,1.385559) min: (-1.421875,-1.4245888)
DS-CNN_conv_ds_2_pw_conv_weights_0 number of wts/bias: (1, 1, 64, 64) dec bits: 7 max: (0.734375,0.7343305) min: (-0.71875,-0.7169189)
DS-CNN_conv_ds_2_pw_conv_biases_0 number of wts/bias: (64,) dec bits: 6 max: (1.171875,1.1795075) min: (-1.203125,-1.2094603)
DS-CNN_conv_ds_3_dw_conv_depthwise_weights_0 number of wts/bias: (3, 3, 64, 1) dec bits: 6 max: (1.671875,1.6702775) min: (-1.421875,-1.4233767)
DS-CNN_conv_ds_3_dw_conv_biases_0 number of wts/bias: (64,) dec bits: 6 max: (1.34375,1.342889) min: (-1.25,-1.2532599)
DS-CNN_conv_ds_3_pw_conv_weights_0 number of wts/bias: (1, 1, 64, 64) dec bits: 7 max: (0.609375,0.6132394) min: (-0.65625,-0.65593976)
DS-CNN_conv_ds_3_pw_conv_biases_0 number of wts/bias: (64,) dec bits: 5 max: (2.03125,2.017891) min: (-1.625,-1.6252948)
DS-CNN_conv_ds_4_dw_conv_depthwise_weights_0 number of wts/bias: (3, 3, 64, 1) dec bits: 6 max: (1.390625,1.3926073) min: (-1.75,-1.7565634)
DS-CNN_conv_ds_4_dw_conv_biases_0 number of wts/bias: (64,) dec bits: 6 max: (1.265625,1.2597218) min: (-1.578125,-1.5803734)
DS-CNN_conv_ds_4_pw_conv_weights_0 number of wts/bias: (1, 1, 64, 64) dec bits: 7 max: (0.9296875,0.93012524) min: (-0.6640625,-0.6612934)
DS-CNN_conv_ds_4_pw_conv_biases_0 number of wts/bias: (64,) dec bits: 6 max: (1.265625,1.2726928) min: (-0.5625,-0.5625464)
DS-CNN_fc1_weights_0 number of wts/bias: (64, 12) dec bits: 6 max: (0.90625,0.9100082) min: (-1.28125,-1.2854961)
DS-CNN_fc1_biases_0 number of wts/bias: (12,) dec bits: 9 max: (0.14648438,0.1462188) min: (-0.0703125,-0.06975072)
"""

mfcc_dec_bits = 1     # number of decimal bits of the input MFCC data

weight_names = [ "DS-CNN_conv_1_weights_0",
                 "DS-CNN_conv_ds_1_dw_conv_depthwise_weights_0",
                 "DS-CNN_conv_ds_1_pw_conv_weights_0",
                 "DS-CNN_conv_ds_2_dw_conv_depthwise_weights_0",
                 "DS-CNN_conv_ds_2_pw_conv_weights_0",
                 "DS-CNN_conv_ds_3_dw_conv_depthwise_weights_0",
                 "DS-CNN_conv_ds_3_pw_conv_weights_0",
                 "DS-CNN_conv_ds_4_dw_conv_depthwise_weights_0",
                 "DS-CNN_conv_ds_4_pw_conv_weights_0",
                 "DS-CNN_fc1_weights_0" ]
bias_names = [ "DS-CNN_conv_1_biases_0",
               "DS-CNN_conv_ds_1_dw_conv_biases_0",
               "DS-CNN_conv_ds_1_pw_conv_biases_0",
               "DS-CNN_conv_ds_2_dw_conv_biases_0",
               "DS-CNN_conv_ds_2_pw_conv_biases_0",
               "DS-CNN_conv_ds_3_dw_conv_biases_0",
               "DS-CNN_conv_ds_3_pw_conv_biases_0",
               "DS-CNN_conv_ds_4_dw_conv_biases_0",
               "DS-CNN_conv_ds_4_pw_conv_biases_0",
               "DS-CNN_fc1_biases_0" ]
out_shift_names = [ "CONV1_OUT_RSHIFT",
                    "CONV2_DS_OUT_RSHIFT",
                    "CONV2_PW_OUT_RSHIFT",
                    "CONV3_DS_OUT_RSHIFT",
                    "CONV3_PW_OUT_RSHIFT",
                    "CONV4_DS_OUT_RSHIFT",
                    "CONV4_PW_OUT_RSHIFT",
                    "CONV5_DS_OUT_RSHIFT",
                    "CONV5_PW_OUT_RSHIFT",
                    "FINAL_FC_OUT_RSHIFT" ]
bias_shift_names = [ "CONV1_BIAS_LSHIFT",
                     "CONV2_DS_BIAS_LSHIFT",
                     "CONV2_PW_BIAS_LSHIFT",
                     "CONV3_DS_BIAS_LSHIFT",
                     "CONV3_PW_BIAS_LSHIFT",
                     "CONV4_DS_BIAS_LSHIFT",
                     "CONV4_PW_BIAS_LSHIFT",
                     "CONV5_DS_BIAS_LSHIFT",
                     "CONV5_PW_BIAS_LSHIFT",
                     "FINAL_FC_BIAS_LSHIFT" ]


def extract_bits(line):
    return int(re.findall(r"dec bits: ([0-9]+)", line)[0])


if __name__ == "__main__":

    layer_cnt = len(weight_names)
    if len(bias_names) != layer_cnt or len(out_shift_names) != layer_cnt or len(bias_shift_names) != layer_cnt or len(act_max) < layer_cnt + 1:
        print("invalid config")
        sys.exit(1)

    curr_layer = 0
    curr_shift = 0
    actmax_idx = 1                # first entry in act_max is the input layer (skip)
    input_bits = mfcc_dec_bits    # layer input decimal bits

    for line in quant_test_output.split('\n'):

        if weight_names[curr_layer] in line:
            print("layer %d input format: Q%d.%d" % (curr_layer, (7 - input_bits), input_bits))
            # extract the decimal bits of the weights and calculate the output shift
            input_bits = input_bits + extract_bits(line)
            req_int_bits = math.ceil(math.log(act_max[actmax_idx], 2))
            curr_shift = (input_bits - (7 - req_int_bits))
            print("%s: %d" % (out_shift_names[curr_layer], curr_shift))
        elif bias_names[curr_layer] in line:
            # extract the decimal bits of the bias and calculate the required bias shift
            bias_bits = extract_bits(line)
            print("%s: %d" % (bias_shift_names[curr_layer], input_bits - bias_bits))
            input_bits = input_bits - curr_shift
            # move on to the next layer
            curr_layer = curr_layer + 1
            actmax_idx = actmax_idx + 1
            if curr_layer + 1 == layer_cnt:
                # skip the pooling layer which comes before the fully-connected layer in act_max:
                req_int_bits = math.ceil(math.log(act_max[actmax_idx], 2))
                print("skipping pooling layer (left shift: %d)" % ((7 - input_bits) - req_int_bits))
                input_bits = 7 - req_int_bits
                actmax_idx = actmax_idx + 1

        if curr_layer == layer_cnt:
            print("final layer output format: Q%d.%d" % ((7 - input_bits), input_bits))
            break

