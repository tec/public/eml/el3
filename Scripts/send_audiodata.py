#!/usr/bin/env python3

"""
Copyright (c) 2021, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.
"""

"""

Sends raw audio data via the serial port to the MCU for inference or training.

Note that the audio files must be in uncompressed wave format (16-bit, 16kHz)
and 1 second in length. Shorter files will be padded with zeros, longer files
will be truncated.

"""

import argparse
import binascii
import math
import os
import random
import re
import serial
import struct
import sys
import time
import wave


payloadlen = 256              # max. packet length, in number of samples
numsamples = 16000            # 1s @ 16kHz
labelsdict = { "silence" : 0, "unknown" : 1, "yes" : 2, "no" : 3, "up" : 4, "down" : 5, "left" : 6, "right" : 7, "on" : 8, "off" : 9, "stop" : 10, "go" : 11 }
frame      = b'\x7e'          # framing byte
escape     = b'\x7d'          # escape byte
commands   = { 'print_gradients': -2, 'print_weights': -3, 'print_quantized': -4 }   # list of available commands for the MCU

FLAGS = None


def connect_to_serial(device, baudr):
    if not isinstance(device, str) or not isinstance(baudr, int):
        print("invalid device or baudrate")
        return None
    if not os.path.exists(device):
        print("device %s not found" % device)
        return None
    try:
        sp = serial.Serial(port=device, baudrate=baudr, timeout=None)
        sp.flushInput()
        sp.flushOutput()
        print("connected to %s" % device)
        return sp
    except serial.SerialException:
        print("device %s unavailable" % device)
    except ValueError:
        print("invalid arguments")
    except OSError:
        print("device %s not found" % device)
    except:
        e = sys.exc_info()[0]
        print("error: %s" % e)
    return None


def update_progress_bar(progress, accuracy=None):
    progbarlen = 40
    progress = min(100, max(progress, 0))
    if accuracy:
        # print current accuracy and progress
        sys.stdout.write('%s[ progress: %5.1f%% | accuracy: %5.1f%% ]' % ('\b'*(progbarlen), progress, accuracy))
    else:
        # print the progress only
        if progbarlen < 6:
            progbarlen = 6
        progrscale = int(progbarlen * progress / 100)
        progbar    = "%s%s" % ('-'*progrscale, ' '*(progbarlen - progrscale))
        percentage = "%3.1f%%" % (progress)
        progbar    = progbar[:int(progbarlen / 2) - 3] + percentage + progbar[int(progbarlen / 2) + 3:]
        sys.stdout.write('%s[%s]' % ('\b'*(progbarlen + 2), progbar))
    sys.stdout.flush()


def remove_progress_bar():
    progbarlen = 40
    sys.stdout.write('\b'*(progbarlen + 2))


def send_packet(conn, payload, seqno, offset, label=None):
    if not conn or not isinstance(conn, serial.Serial) or not conn.isOpen():
        print("invalid serial connection")
        return False
    if label is None or label not in labelsdict:
        label = -1
    else:
        label = labelsdict[label]
    # compose the packet
    crc     = binascii.crc32(payload)
    header  = struct.pack('<HHIh', seqno, offset, crc, label)
    pkt     = header + payload
    # mask (escape) all occurrances of framing and escape byte within the packet
    pkt_esc = pkt.replace(escape, escape + escape).replace(frame, escape + frame)
    pkt_fr  = frame + pkt_esc + frame
    # send the packet
    conn.write(pkt_fr)
    # wait and read response (if any)
    time.sleep(0.002)
    if conn.inWaiting() > 0 and FLAGS.debug:
        print(conn.read(conn.inWaiting()).decode('UTF-8'))
    return True


def send_command(conn, cmd):
    if not conn or not isinstance(conn, serial.Serial) or not conn.isOpen():
        print("invalid serial connection")
        return False
    # no payload, no CRC
    pkt = struct.pack('<HHIh', 0, numsamples, 0, cmd)
    # mask (escape) all occurrances of framing and escape byte within the packet
    pkt_esc = pkt.replace(escape, escape + escape).replace(frame, escape + frame)
    pkt_fr  = frame + pkt_esc + frame
    # send the packet
    conn.write(pkt_fr)
    # wait and read response (if any)
    timeout = 1000
    while timeout > 0:
        if conn.inWaiting() > 0:
            print(conn.read(conn.inWaiting()).decode('UTF-8'), end='')
        time.sleep(0.001)
        timeout = timeout - 1
    return True


# if label is provided, function returns true if the label matches
# if no label is provided, the function returns true if the operation was successful
def send_waveform(conn, data, show_progress=False, label=None):
    if not conn or not isinstance(conn, serial.Serial) or not conn.isOpen():
        remove_progress_bar()
        print("invalid serial connection")
        return False
    if not isinstance(data, list) or len(data) < numsamples:
        remove_progress_bar()
        print("invalid waveform length (must contain %u samples)" % numsamples)
        return False
    if FLAGS.debug:
        print("sending audio data...")
    # partition the data into chunks of length 'payloadlen'
    seqno = 0
    numchunks = math.ceil(numsamples / payloadlen)
    for i in range(0, numchunks):
        offset = i * payloadlen
        count  = payloadlen
        if numsamples - offset < count:
            count = numsamples - offset
        #print("sending packet with seqno %u... (offset: %u, samples: %u)" % (seqno, offset, count))
        if show_progress:
            progress = (i + 1) * 100 / numchunks
            update_progress_bar(progress)
        data_enc = struct.pack("<%dh" % count, *data[offset:offset+payloadlen])
        if not send_packet(conn, data_enc, seqno, offset, label if label and FLAGS.send_labels else None):
            return False
        seqno = seqno + 1
    # give the MCU some time to run the inference
    if FLAGS.debug:
        sys.stdout.write("\r\nwaiting for inference result...\r\n")
        sys.stdout.flush()
    timeout = 1000    # in ms
    while conn.inWaiting() == 0 and timeout:
        time.sleep(0.001)
        timeout = timeout - 1
    if conn.inWaiting():
        time.sleep(0.01)
        mcu_response = conn.read(conn.inWaiting()).decode('UTF-8')
        if not FLAGS.debug and 'error' in mcu_response.lower():
            remove_progress_bar()
            print(mcu_response)
        if label:
            if FLAGS.debug:
                print(mcu_response.strip())
            matches = re.findall(r"detected '([^']+)'.*", mcu_response)
            return label in matches
        else:
            print(mcu_response.strip())
            return True
    else:
        if label:
            raise Exception("timeout, no response received")
        else:
            return False


def load_audiodata(filename):
    audiodata = []
    with wave.open(filename, 'r') as wf:
        nsamples = wf.getnframes()
        channels = wf.getnchannels()
        samplewidth = wf.getsampwidth()
        if channels != 1:
            remove_progress_bar()
            print("invalid number of channels (%u instead of 1)" % channels)
            return None
        if samplewidth != 2:
            remove_progress_bar()
            print("invalid sample width (%u instead of 2 bytes)" % samplewidth)
            return None
        if nsamples != numsamples and FLAGS.debug:
            remove_progress_bar()
            print("audio file contains %u samples (%u expected)" % (nsamples, numsamples))
        wavedata  = wf.readframes(nsamples)
        # crop or pad the file
        if nsamples > numsamples:
            wavedata = wavedata[:numsamples * 2]
            remove_progress_bar()
            if FLAGS.debug:
                print("  %d samples removed from file '%s'" % (nsamples - numsamples, filename))
        elif nsamples < numsamples:
            wavedata += bytearray([0] * (numsamples - nsamples) * 2)
            remove_progress_bar()
            if FLAGS.debug:
                print("  %d padding samples added to file '%s'" % (numsamples - nsamples, filename))
        audiodata.extend(struct.unpack("<%dh" % numsamples, wavedata))
    return audiodata



if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-f',
        '--filename',
        type=str,
        default=None,
        help='path to a single audio file (wav format)')
    parser.add_argument(
        '-d',
        '--directory',
        type=str,
        default=None,
        help='path to a directory that contains audio files')
    parser.add_argument(
        '-x',
        '--listfile',
        type=str,
        default=None,
        help='a CSV file that contains a list of audio filename and label pairs')
    parser.add_argument(
        '-l',
        '--label',
        type=str,
        default=None,
        help='the label for a single audio file')
    parser.add_argument(
        '--send_labels',
        action="store_true",
        default=False,
        help='whether to send the correct labels along with the audio data to the MCU')
    parser.add_argument(
        '--show_progressbar',
        action="store_true",
        default=False,
        help='whether to show a progress bar instead of the current accuracy')
    parser.add_argument(
        '-b',
        '--baudrate',
        type=int,
        default=1000000,
        help='baudrate for the serial connection to the MCU')
    parser.add_argument(
        '-p',
        '--port',
        type=str,
        default='/dev/ttyACM0',
        help='serial port / device to connect to')
    parser.add_argument(
        '--debug',
        action="store_true",
        default=False,
        help='the label for a single audio file')
    parser.add_argument(
        '-c',
        '--command',
        type=str,
        default='',
        help='send a command to the MCU (available commands: ' + ', '.join([key for key in commands]) + ')')
    FLAGS, unparsed = parser.parse_known_args()

    # compose a list of filenames and labels
    files  = []
    labels = []

    if FLAGS.filename:
        if not "wav" in FLAGS.filename or not os.path.isfile(FLAGS.filename):
            print("invalid audio file '%s'" % FLAGS.filename)
            sys.exit(1)
        files.append(FLAGS.filename)

    if FLAGS.label:
        labels.append(FLAGS.label)

    if FLAGS.directory:
        if not os.path.isdir(FLAGS.directory):
            print("invalid directory '%s'" % FLAGS.directory)
            sys.exit(1)
        # get a file list
        files = [ os.path.join(FLAGS.directory, f) for f in os.listdir(FLAGS.directory) if os.path.isfile(os.path.join(FLAGS.directory, f)) ]
        print("%d audio files found in '%s'" % (len(files), FLAGS.directory))

    if FLAGS.listfile:
        if not os.path.isfile(FLAGS.listfile):
            print("file '%s' not found" % FLAGS.listfile)
            sys.exit(1)
        # a csv file that contains filenames and labels (format:  filename,label)
        try:
            for line in open(FLAGS.listfile, 'r'):
                parts = line.strip().split(',')
                if len(parts) != 2:
                    continue   # skip (line must contain a filename and label, separated with a comma)
                if os.path.isfile(parts[0]):
                    files.append(parts[0])
                    labels.append(parts[1])
            print("%d filenames with labels found in '%s'" % (len(files), FLAGS.listfile))
        except UnicodeDecodeError:
            print("'%s' is not a valid list file" % FLAGS.listfile)
            sys.exit(1)

    if not files and not FLAGS.command:
        print("no files provided\r\n")
        parser.print_help()
        sys.exit(1)

    # establish connection to MCU via serial port (UART)
    sc = connect_to_serial(FLAGS.port, FLAGS.baudrate)
    if not sc:
        sys.exit(2)
    time.sleep(0.1)
    sc.reset_input_buffer()         # discard input
    sc.reset_output_buffer()        # discard output

    # is there a command to send?
    if FLAGS.command:
        if FLAGS.command not in commands:
            print("unknown command '%s'" % FLAGS.command)
            sys.exit(1)
        send_command(sc, commands[FLAGS.command])
        if not files:
            sys.exit()

    # process all files
    num_files   = len(files)
    correct_cnt = 0
    wrong_cnt   = 0
    total_cnt   = 0
    starttime   = time.time()
    indices     = list(range(num_files))
    random.shuffle(indices)
    for i in indices:
        # load the wav file
        if FLAGS.debug:
            print("loading audio file '%s'..." % files[i])
        audiodata = load_audiodata(files[i])
        if not audiodata:
            continue

        label = labels[i] if labels else None
        try:
            result = send_waveform(sc, audiodata, FLAGS.debug, label)
        except KeyboardInterrupt:
            print("\r\naborted by user")
            break
        except Exception:
            print("\r\nfailed to send audio data (file: %s, error: %s)" % (files[i], str(sys.exc_info()[1])))
            break
        if label:
            if result:
                correct_cnt = correct_cnt + 1
            else:
                wrong_cnt = wrong_cnt + 1
        elif not result:
            remove_progress_bar()
            print("failed to send audio data")

        total_cnt = total_cnt + 1
        progress = (total_cnt) * 100 / num_files
        accuracy = None
        if label:
            if not FLAGS.show_progressbar:
                accuracy = (correct_cnt * 100 / (correct_cnt + wrong_cnt))
            if not FLAGS.debug:
                update_progress_bar(progress, accuracy)
        else:
            print("overall progress: %.1f%%" % (progress))
            if accuracy:
                print("current accuracy: %.1%%" % (accuracy))

    print("\r\n%u audio files processed in %.1fs" % (total_cnt, time.time() - starttime))

    if labels and (correct_cnt + wrong_cnt) > 0:
        print("accuracy: %.2f%%" % (correct_cnt * 100 / (correct_cnt + wrong_cnt)))

    sc.close()
