#!/usr/bin/env python3

"""
Copyright (c) 2021, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# generates an augmented speech data set based on
# http://download.tensorflow.org/data/speech_commands_v0.02.tar.gz

import argparse
import os
import sys
import tarfile
import sox

from six.moves import urllib


FLAGS = None


def maybe_download_and_extract_dataset(data_url, dest_directory):
    """Download and extract data set tar file.

    If the data set we're using doesn't already exist, this function
    downloads it from the TensorFlow.org website and unpacks it into a
    directory.
    If the data_url is none, don't download anything and expect the data
    directory to contain the correct files already.

    Args:
        data_url: Web location of the tar file containing the data set.
        dest_directory: File path to extract data to.
    """
    if not data_url:
        return
    if not os.path.exists(dest_directory):
        os.makedirs(dest_directory)
    filename = data_url.split('/')[-1]
    filepath = os.path.join(dest_directory, filename)
    if not os.path.exists(filepath):

        def _progress(count, block_size, total_size):
            sys.stdout.write(
                '\r>> Downloading %s %.1f%%' %
                (filename, float(count * block_size) / float(total_size) * 100.0))
            sys.stdout.flush()

        try:
            filepath, _ = urllib.request.urlretrieve(data_url, filepath, _progress)
        except:
            print('Failed to download URL: %s to folder: %s', data_url,
                             filepath)
            print('Please make sure you have enough free space and'
                             ' an internet connection')
            raise

        statinfo = os.stat(filepath)
        print('\r\nSuccessfully downloaded %s (%d bytes)' % (filename, statinfo.st_size))
        tarfile.open(filepath, 'r:gz').extractall(dest_directory)
        print('Successfully extracted all files')


def augment_dataset(source_directory, augmented_directory):
    """Augment data set.

    If the augmented data set we want to use doesn't already exist,
    this function augments the un-augmented data set and stores the
    augmented samples into a new directory.
    If the augmented_directory is none, don't download anything and expect
    the data directory to contain the correct files already.

    Args:
        source_directory: File path containing the un-augmented data set.
        augmented_directory: File path to store the augmented data to.
    """
    if not augmented_directory:
        return
    if not os.path.exists(augmented_directory):
        os.makedirs(augmented_directory)
    if not os.path.exists(source_directory):
        print('Source directory does not exist')
        raise
    tfm = sox.Transformer()
    tfm.reverb(wet_only=True)
    print('Applying following Sox effect(s): %s' % tfm.effects_log)

    for dirpath, dirnames, filenames in os.walk(source_directory):
        structure = os.path.join(augmented_directory, dirpath[len(source_directory) + 1:])
        if not os.path.isdir(structure):
            print(structure)
            os.mkdir(structure)
        for filename in filenames:
            if filename.endswith('.wav'):
                tfm.build_file(os.path.join(dirpath, filename), os.path.join(structure, filename))

    print('Successfully augmented the data set')


def create_augmented_dataset():
    maybe_download_and_extract_dataset(FLAGS.data_url, FLAGS.data_dir)
    augment_dataset(FLAGS.data_dir, FLAGS.augmented_data_dir)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--data_url',
        type=str,
        default='http://download.tensorflow.org/data/speech_commands_v0.02.tar.gz',
        help='Location of speech training data archive on the web.')
    parser.add_argument(
        '--data_dir',
        type=str,
        default='/tmp/speech_dataset/',
        help="""\
          Where to store the raw speech training data.
          """)
    parser.add_argument(
        '--augmented_data_dir',
        type=str,
        default='/tmp/speech_dataset_augmented/',
        help="""\
          Where to store the augmented speech training data.
          """)
    FLAGS, unparsed = parser.parse_known_args()
    create_augmented_dataset()
