#!/usr/bin/env python3

"""
Copyright (c) 2021, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.
"""

# generates a list of audio filename + label pairs from a dataset

import argparse
import hashlib
import os
import random


FLAGS = None


# determine the set (training, validation, test) based on the filename (function is based on 'which_set' from tensorflow)
def determine_set(filename):
    MAX_NUM_WAVS_PER_CLASS = 2**27 - 1
    filename = filename[0:8]  # only use the first 8 digits of the filename (which is the hash of the speaker)
    hash_name_hashed = hashlib.sha1(filename.encode("utf8")).hexdigest()  # hash the filename
    percentage_hash = ( (int(hash_name_hashed, 16) % (MAX_NUM_WAVS_PER_CLASS + 1)) * (100.0 / MAX_NUM_WAVS_PER_CLASS) )
    if percentage_hash < FLAGS.validation_percentage:
        return 'validation'
    elif percentage_hash < (FLAGS.testing_percentage + FLAGS.validation_percentage):
        return 'testing'
    return 'training'


def create_filelist(save_to_file=True, shuffle=True):
    filelist = { 'testing': [], 'validation': [], 'training': [] }
    classes = FLAGS.classes.split(',')
    for c in classes:
        subdir = os.path.join(FLAGS.dataset_path, c)
        if not os.path.isdir(subdir):
            print("directory %s not found" % subdir)
            continue
        for filename in os.listdir(subdir):
            filename_abs = os.path.abspath(os.path.join(subdir, filename))
            if os.path.isfile(filename_abs) and os.path.splitext(filename)[1] == ".wav":
                filelist[determine_set(filename)].append(filename_abs + "," + c)
    if save_to_file:
        num_files = sum(len(filelist[x]) for x in filelist)
        if num_files == 0:
            print("no files found")
            return None
        for file_set in filelist:
            with open(file_set + ".csv", "w") as f:
                if shuffle:
                    random.shuffle(filelist[file_set])
                f.write("\r\n".join(filelist[file_set]))
            print("%s set: %d files (%.1f%%)" % (file_set, len(filelist[file_set]), len(filelist[file_set]) * 100.0 / num_files))
    return filelist


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--dataset_path',
        required=True,
        type=str,
        help='location of the speech dataset')
    parser.add_argument(
        '--classes',
        default="yes,no,up,down,left,right,on,off,stop,go",
        type=str,
        help='location of the speech dataset')
    parser.add_argument(
        '--testing_percentage',
        type=int,
        default=20,
        help="""\
          percentage of files in the test set
          """)
    parser.add_argument(
        '--validation_percentage',
        type=int,
        default=20,
        help="""\
          percentage of files in the validation set
          """)
    FLAGS, unparsed = parser.parse_known_args()
    create_filelist()
